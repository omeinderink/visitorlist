build:	Dockerfile
	docker build -t visitorlist .

run:
	docker run --rm -d -p 8080:80 --name visitorlist visitorlist:latest

stop:
	docker stop -t 1 visitorlist
