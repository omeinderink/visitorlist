FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL C

RUN apt update && apt install -y \
    apache2 \
&&  apt-get clean \
&&  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY --chown=www-data:www-data index.html /var/www/html/index.html
COPY --chown=www-data:www-data cgi.sh /usr/lib/cgi-bin/cgi.sh

RUN a2enmod cgid

EXPOSE 80/tcp

CMD ["apachectl", "-D", "FOREGROUND"]
