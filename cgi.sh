#!/bin/bash

# disable filename globbing
set -f

echo "Content-type: text/plain; charset=iso-8859-1"
echo

if [ "$REQUEST_METHOD" = "POST" ]; then
    if [ "$CONTENT_LENGTH" -gt 0 ]; then
        read -n $CONTENT_LENGTH POST_DATA <&0
    fi
fi
echo "Thank you. The following visitor was registered in the system:"

FIRST_NAME=$(echo $POST_DATA | cut -d '&' -f1 | cut -d '=' -f2)
LAST_NAME=$(echo $POST_DATA | cut -d '&' -f2 | cut -d '=' -f2)
echo "first name: $FIRST_NAME"
echo "last name: $LAST_NAME"

echo "${FIRST_NAME},${LAST_NAME}" >> /tmp/visitorlist.txt

echo "the database at /tmp/visitorlist.txt currently contains the following entries:"
cat /tmp/visitorlist.txt
